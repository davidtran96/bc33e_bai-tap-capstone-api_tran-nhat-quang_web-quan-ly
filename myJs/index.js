import {
  getFormInformation,
  renderProductList,
  showInfomation,
} from "./productAdminController.js";
import { Product } from "./productAdminModel.js";
import { varlidator } from "./validatorAdmin.js";

const BASE_URL = "https://62f8b755e0564480352bf404.mockapi.io/Products";

let productList = [];

// Loading
let onLoading = () => {
  document.getElementById("loading").style.display = "flex";
};

let offLoading = () => {
  document.getElementById("loading").style.display = "none";
};

// Render dữ liệu ra giao diện
let renderTableSevice = () => {
  onLoading();
  axios({
    url: `${BASE_URL}`,
    method: "GET",
  })
    .then((res) => {
      console.log("res: ", res);
      offLoading();
      renderProductList(res.data);

      // Lấy dữ liệu push vào producList để xử lý
      res.data.forEach((product) => {
        productList.push(product);
      });
    })
    .catch((res) => {
      console.log("res: ", res);
      offLoading();
    });
};
renderTableSevice();

// Thêm sản phẩm
let addProduct = () => {
  onLoading();
  let dataForm = getFormInformation();
  let product = new Product(
    dataForm.id,
    dataForm.name,
    dataForm.price,
    dataForm.screen,
    dataForm.backCamera,
    dataForm.frontCamera,
    dataForm.img,
    dataForm.desc,
    dataForm.type
  );
  // varlidator name
  var isValid =
    varlidator.kiemTraRong(product.name, "tbTen") &&
    varlidator.kiemTraTenSanPhamTrungLap(product.name, productList);

  // varlidator price
  isValid = isValid & varlidator.kiemTraRong(product.price, "tbGia");

  // varlidator screen
  isValid = isValid & varlidator.kiemTraRong(product.screen, "tbManHinh");
  // varlidator backCamera
  isValid =
    isValid & varlidator.kiemTraRong(product.backCamera, "tbCameraTruoc");
  // varlidator frontCamera
  isValid =
    isValid & varlidator.kiemTraRong(product.frontCamera, "tbCameraSau");
  // varlidator img
  isValid = isValid & varlidator.kiemTraRong(product.img, "tbHinhAnh");
  // varlidator desc
  isValid = isValid & varlidator.kiemTraRong(product.desc, "tbMoTa");
  // varlidator type
  isValid = isValid & varlidator.kiemTraRong(product.type, "tbLoai");

  if (isValid == false) {
    offLoading();
    return;
  }
  axios({
    url: `${BASE_URL}`,
    method: "POST",
    data: product,
  })
    .then((res) => {
      console.log("res: ", res);
      offLoading();
      renderTableSevice();
    })
    .catch((res) => {
      console.log("res: ", res);
      offLoading();
    });
};
window.addProduct = addProduct;

// Xóa sản phẩm
let deleteProduct = (id) => {
  onLoading();
  axios({
    url: `${BASE_URL}/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      console.log("res: ", res);
      offLoading();
      renderTableSevice();
    })
    .catch((err) => {
      console.log("err: ", err);
      offLoading();
    });
};
window.deleteProduct = deleteProduct;

// Sửa sản phẩm
let modifyProduct = (id) => {
  onLoading();
  axios({
    url: `${BASE_URL}/${id}`,
    method: "GET",
  })
    .then((res) => {
      console.log("res: ", res);
      offLoading();
      showInfomation(res.data);
    })
    .catch((err) => {
      console.log("err: ", err);
      offLoading();
    });
};
window.modifyProduct = modifyProduct;

// Cập nhật sản phẩm
let updateProduct = () => {
  onLoading();
  let dataForm = getFormInformation();
  let updateProduct = new Product(
    dataForm.id,
    dataForm.name,
    dataForm.price,
    dataForm.screen,
    dataForm.backCamera,
    dataForm.frontCamera,
    dataForm.img,
    dataForm.desc,
    dataForm.type
  );

  // varlidator name
  var isValid = varlidator.kiemTraRong(updateProduct.name, "tbTen");
  // varlidator price
  isValid = isValid & varlidator.kiemTraRong(updateProduct.price, "tbGia");

  // varlidator screen
  isValid = isValid & varlidator.kiemTraRong(updateProduct.screen, "tbManHinh");
  // varlidator backCamera
  isValid =
    isValid & varlidator.kiemTraRong(updateProduct.backCamera, "tbCameraTruoc");
  // varlidator frontCamera
  isValid =
    isValid & varlidator.kiemTraRong(updateProduct.frontCamera, "tbCameraSau");
  // varlidator img
  isValid = isValid & varlidator.kiemTraRong(updateProduct.img, "tbHinhAnh");
  // varlidator desc
  isValid = isValid & varlidator.kiemTraRong(updateProduct.desc, "tbMoTa");
  // varlidator type
  isValid = isValid & varlidator.kiemTraRong(updateProduct.type, "tbLoai");

  if (isValid == false) {
    offLoading();
    return;
  }

  let _id = dataForm.id;
  axios({
    url: `${BASE_URL}/${_id}`,
    method: "PUT",
    data: updateProduct,
  })
    .then((res) => {
      console.log("res: ", res);
      offLoading();
      renderTableSevice();
    })
    .catch((err) => {
      console.log("err: ", err);
      offLoading();
    });
};
window.updateProduct = updateProduct;

let resetForm = () => {
  document.getElementById("modal__form").reset();
};
window.resetForm = resetForm;
