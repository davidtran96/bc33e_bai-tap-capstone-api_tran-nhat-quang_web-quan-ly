// render danh sách sản phẩm
export let renderProductList = (list) => {
  let contentHTML = "";

  list.forEach((item) => {
    let content = `
    <tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td>${item.price}</td>
        <td>${item.screen}</td>
        <td>${item.backCamera}</td>
        <td>${item.frontCamera}</td>
        <td>
            <img src="${item.img}">
        </td>
        <td>${item.desc}</td>
        <td>${item.type}</td>
        <td>
            <div style="display: flex; justify-content: space-between; align-items: center">
                <button class="btn btn-danger" onclick="deleteProduct('${item.id}')">Xóa</button>
                <button class="btn btn-warning" data-toggle="modal" data-target="#myModal" onclick="modifyProduct('${item.id}')">Sửa</button>
            </div>
        </td>
    </tr>
    `;
    contentHTML += content;
  });
  document.getElementById("tableProduct").innerHTML = contentHTML;
};

// Lấy thông tin từ form
export let getFormInformation = () => {
  let id = document.getElementById("id").value.trim();
  let name = document.getElementById("name").value.trim();
  let price = document.getElementById("price").value.trim();
  let screen = document.getElementById("screen").value.trim();
  let backCamera = document.getElementById("backCamera").value.trim();
  let frontCamera = document.getElementById("frontCamera").value.trim();
  let img = document.getElementById("img").value.trim();
  let desc = document.getElementById("desc").value.trim();
  let type = document.getElementById("type").value.trim();

  return {
    id,
    name,
    price,
    screen,
    backCamera,
    frontCamera,
    img,
    desc,
    type,
  };
};

// hiển thị thông tin lên form
export let showInfomation = (data) => {
  document.getElementById("id").value = data.id;
  document.getElementById("name").value = data.name;
  document.getElementById("price").value = data.price;
  document.getElementById("screen").value = data.screen;
  document.getElementById("backCamera").value = data.backCamera;
  document.getElementById("frontCamera").value = data.frontCamera;
  document.getElementById("img").value = data.img;
  document.getElementById("desc").value = data.desc;
  document.getElementById("type").value = data.type;
};
